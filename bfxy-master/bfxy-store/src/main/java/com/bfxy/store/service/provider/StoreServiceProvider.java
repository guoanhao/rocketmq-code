package com.bfxy.store.service.provider;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.bfxy.store.api.StoreServiceApi;
import com.bfxy.store.mapper.StoreMapper;

/**
 * 对外提供的接口
 * 实现了bfxy-store-api中的接口StoreServiceApi
 */
//注册成dubbo的服务--对外提供者
@Service(
        version = "1.0.0",
        application = "${dubbo.application.id}",
        protocol = "${dubbo.protocol.id}",
        registry = "${dubbo.registry.id}"
)
public class StoreServiceProvider implements StoreServiceApi {

    @Autowired
    private StoreMapper storeMapper;

    /**
     * 查询版本号
     *
     * @param supplierId 商户ID
     * @param goodsId    商品ID
     * @return
     */
    @Override
    public int selectVersion(String supplierId, String goodsId) {
        return storeMapper.selectVersion(supplierId, goodsId);
    }

    /**
     * 更新库存
     *
     * @param version    版本号
     * @param supplierId 商户ID
     * @param goodsId    商品ID
     * @param updateBy   更新人
     * @param updateTime 更新时间
     * @return
     */
    @Override
    public int updateStoreCountByVersion(int version, String supplierId, String goodsId, String updateBy,
                                         Date updateTime) {
        return storeMapper.updateStoreCountByVersion(version, supplierId, goodsId, updateBy, updateTime);
    }

    /**
     * 查询库存
     *
     * @param supplierId 商户ID
     * @param goodsId    商品ID
     * @return
     */
    @Override
    public int selectStoreCount(String supplierId, String goodsId) {
        return storeMapper.selectStoreCount(supplierId, goodsId);
    }

}
