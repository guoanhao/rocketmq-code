package com.bfxy.paya.service;

public interface PayService {
	/**
	 *  接口
	 * @param userId		用戶ID
	 * @param orderId		訂單ID
	 * @param accountId		賬號ID
	 * @param money			金額
	 * @return
	 */
	String payment(String userId, String orderId, String accountId, double money);
}
