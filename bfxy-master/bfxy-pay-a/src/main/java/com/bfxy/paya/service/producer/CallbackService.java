package com.bfxy.paya.service.producer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bfxy.paya.utils.FastJsonConvertUtil;

@Service
public class CallbackService {

	// 通過主題和tags去給Order發消息
	// 主題
	public static final String CALLBACK_PAY_TOPIC = "callback_pay_topic";

	// tags
	public static final String CALLBACK_PAY_TAGS = "callback_pay";
	
	public static final String NAMESERVER = "192.168.40.201:9876;192.168.40.202:9876;192.168.40.203:9876;192.168.40.204:9876;";

	// 同步的發送
	@Autowired
	private SyncProducer syncProducer;

	/**
	 * 給訂單（order）那邊發確認消息，告訴訂單Order系統
	 * @param orderId	訂單ID
	 * @param userId
	 */
	public void sendOKMessage(String orderId, String userId) {
		
		Map<String, Object> params = new HashMap<>();
		params.put("userId", userId);
		params.put("orderId", orderId);
		params.put("status", "2");	//ok，狀態
		
		String keys = UUID.randomUUID().toString() + "$" + System.currentTimeMillis();//消息的key
		// 序列化成json
		Message message = new Message(CALLBACK_PAY_TOPIC, CALLBACK_PAY_TAGS, keys, FastJsonConvertUtil.convertObjectToJSON(params).getBytes());

		// 給訂單那邊發消息
		SendResult ret = syncProducer.sendMessage(message);	
	}
	

	
}
