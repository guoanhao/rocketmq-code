package com.bfxy.paya.service.producer;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.apache.rocketmq.client.producer.LocalTransactionState;
import org.apache.rocketmq.client.producer.TransactionListener;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bfxy.paya.mapper.CustomerAccountMapper;
@Component
public class TransactionListenerImpl implements TransactionListener {

	// 用戶賬戶
	@Autowired
	private CustomerAccountMapper customerAccountMapper;

	/**
	 * 用於執行本地事物
	 * @param msg
	 * @param arg
	 * @return
	 */
	@Override
	public LocalTransactionState executeLocalTransaction(Message msg, Object arg) {
		System.err.println("执行本地事务单元------------");
		CountDownLatch currentCountDown = null;
		try {
			Map<String, Object> params = (Map<String, Object>) arg;
			String userId = (String)params.get("userId");
			String accountId = (String)params.get("accountId");
			String orderId = (String)params.get("orderId");
			BigDecimal payMoney = (BigDecimal)params.get("payMoney");	//	当前的支付款
			BigDecimal newBalance = (BigDecimal)params.get("newBalance");	//	前置扣款成功的余额，支付後的餘額
			int currentVersion = (int)params.get("currentVersion");// 版本還，樂觀鎖
			currentCountDown = (CountDownLatch)params.get("currentCountDown");// 同步阻塞
		
			//updateBalance 传递当前的支付款 数据库操作: 
			Date currentTime = new Date();
			int count = this.customerAccountMapper.updateBalance(accountId, newBalance, currentVersion, currentTime);//更新餘額。利用樂觀鎖
			if(count == 1) {
				// 本地事物執行完成，放行本地事物，在PayServiceImpl中countDownLatch.await();就可以繼續往下走了
				currentCountDown.countDown();
				return LocalTransactionState.COMMIT_MESSAGE;//成功
			} else {
				currentCountDown.countDown();
				return LocalTransactionState.ROLLBACK_MESSAGE;//回滾
			}
		} catch (Exception e) {
			e.printStackTrace();
			currentCountDown.countDown();
			return LocalTransactionState.ROLLBACK_MESSAGE;//回滾
		}
		
	}

	/**
	 * 用於回調check
	 * @param msg
	 * @return
	 */
	@Override
	public LocalTransactionState checkLocalTransaction(MessageExt msg) {
		// TODO Auto-generated method stub
		return null;
	}

}
