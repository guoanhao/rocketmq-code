package com.bfxy.order.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bfxy.order.service.OrderService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@RestController
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	//超时降级
//	@HystrixCommand(
//				commandKey = "createOrder",//与下面的接口@RequestMapping("/createOrder")相同
//				//配置项
//				commandProperties = {
//						//第一个key(name)-value
//						@HystrixProperty(name="execution.timeout.enabled", value="true"),
//						//第二个key(name)-value，超时的一个时间
//						@HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds", value="3000"),
//				},
//				//如果超时了，进行降级。如果三秒钟@RequestMapping("/createOrder")这个方法
//				//还不成功的话，就会走所降级的方法：createOrderFallbackMethod4Timeout，
//				//这个降级的方法一定要跟原来的方法一模一样。
//				fallbackMethod = "createOrderFallbackMethod4Timeout"
//			)
	
	//限流策略：线程池方式
//	@HystrixCommand(
//				commandKey = "createOrder",
//				commandProperties = {
//				//使用的策略叫THREAD（线程池）方式的策略
//						@HystrixProperty(name="execution.isolation.strategy", value="THREAD")
//				},
//				//指定程池的key
//				threadPoolKey = "createOrderThreadPool",
//				//对线程池的配置
//				threadPoolProperties = {
//				 		//核心线程数量，相当于初始化的线程，初始化线程的时候允许有10个链接
//						@HystrixProperty(name="coreSize", value="10"),
//						//有界队列的大小，可以设置的很大，在实际用的时候只需要设置queueSizeRejectionThreshold的值就可以了，动态的将参数进行变更。
//						@HystrixProperty(name="maxQueueSize", value="20000"),
//						//拒绝允许上限的阈值，这个参数可以进行动态地调整，在实际的工作中这个参数设最关键的。
//						@HystrixProperty(name="queueSizeRejectionThreshold", value="30")
//				},
//				//假如不满足规则了，就调用 createOrderFallbackMethod4Thread 方法
//				fallbackMethod="createOrderFallbackMethod4Thread"
//			)
	
	//	限流策略：信号量方式
//	@HystrixCommand(
//				commandKey="createOrder",
//				commandProperties= {
//						//方式为：SEMAPHORE
//						@HystrixProperty(name="execution.isolation.strategy", value="SEMAPHORE"),
//						//限制一次最多并发三个
//						@HystrixProperty(name="execution.isolation.semaphore.maxConcurrentRequests", value="3")
//				},
//				//一次超过三个请求，就走降级
//				fallbackMethod = "createOrderFallbackMethod4semaphore"
//			)

	/**
	 * 我们创建createOrder接口，规范：controller/method/version（版本号）
	 * @param cityId		城市ID
	 * @param platformId	平台ID
	 * @param userId		用户ID
	 * @param supplierId	商户ID
	 * @param goodsId		商品ID
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/createOrder")
	public String createOrder(@RequestParam("cityId")String cityId, 
			@RequestParam("platformId")String platformId,
			@RequestParam("userId")String userId,
			@RequestParam("supplierId")String supplierId,
			@RequestParam("goodsId")String goodsId) throws Exception {
		//Thread.sleep(5000);//睡眠五秒钟，测试会不会走降级后的方法。
		return orderService.createOrder(cityId, platformId, userId, supplierId, goodsId) ? "下单成功!" : "下单失败!";
	}

	//超时 降级的方法
	public String createOrderFallbackMethod4Timeout(@RequestParam("cityId")String cityId, 
			@RequestParam("platformId")String platformId,
			@RequestParam("userId")String userId,
			@RequestParam("suppliedId")String suppliedId,
			@RequestParam("goodsId")String goodsId) throws Exception {
		System.err.println("-------超时降级策略执行------------");
		return "hysrtix timeout !";
	}
	//线程池方式 降级方法
	public String createOrderFallbackMethod4Thread(@RequestParam("cityId")String cityId, 
			@RequestParam("platformId")String platformId,
			@RequestParam("userId")String userId,
			@RequestParam("suppliedId")String suppliedId,
			@RequestParam("goodsId")String goodsId) throws Exception {
		System.err.println("-------线程池限流降级策略执行------------");
		return "hysrtix threadpool !";
	}
	
	public String createOrderFallbackMethod4semaphore(@RequestParam("cityId")String cityId, 
			@RequestParam("platformId")String platformId,
			@RequestParam("userId")String userId,
			@RequestParam("suppliedId")String suppliedId,
			@RequestParam("goodsId")String goodsId) throws Exception {
		System.err.println("-------信号量限流降级策略执行------------");
		return "hysrtix semaphore !";
	}
}
