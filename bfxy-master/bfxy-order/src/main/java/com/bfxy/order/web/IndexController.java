package com.bfxy.order.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    @RequestMapping(name = "/index")
    public String index() throws Exception {
        System.out.println("--------");
        return "index";
    }
}
