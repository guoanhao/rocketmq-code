package com.bfxy.order.service.consumer;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageConst;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bfxy.order.constants.OrderStatus;
import com.bfxy.order.mapper.OrderMapper;
import com.bfxy.order.service.OrderService;
import com.bfxy.order.utils.FastJsonConvertUtil;

/**
 * 用来监听payA，
 * payA支付成功之後會發送一條成功的消息過來
 */
@Component
public class OrderConsumer {

	private DefaultMQPushConsumer consumer;

	// 訂閱的主題
	public static final String CALLBACK_PAY_TOPIC = "callback_pay_topic";
	
	public static final String CALLBACK_PAY_TAGS = "callback_pay";

	public static final String NAMESERVER = "192.168.40.201:9876;192.168.40.202:9876;192.168.40.203:9876;192.168.40.204:9876;";

	@Autowired
	private OrderMapper orderMapper;
	
	@Autowired
	private OrderService orderService;
	
	public OrderConsumer() throws MQClientException {
		consumer = new DefaultMQPushConsumer("callback_pay_consumer_group");
        consumer.setConsumeThreadMin(10);
        consumer.setConsumeThreadMax(50);
        consumer.setNamesrvAddr(NAMESERVER);
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET);
        consumer.subscribe(CALLBACK_PAY_TOPIC, CALLBACK_PAY_TAGS);
        consumer.registerMessageListener(new MessageListenerConcurrently4Pay());
        consumer.start();
	}
	
	class MessageListenerConcurrently4Pay implements MessageListenerConcurrently {

		@Override
		public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
        	MessageExt msg = msgs.get(0);
        	try {
				String topic = msg.getTopic();
				String msgBody = new String(msg.getBody(), "utf-8");
				String tags = msg.getTags();
				String keys = msg.getKeys();	
				System.err.println("收到消息：" + "  topic :" + topic + "  ,tags : " + tags + "keys :" + keys + ", msg : " + msgBody);
				String orignMsgId = msg.getProperties().get(MessageConst.PROPERTY_ORIGIN_MESSAGE_ID);
				System.err.println("orignMsgId: " + orignMsgId);
				
				//通过keys 进行去重表去重 或者使用redis进行去重???? --> 不需要。因為智利之需要改訂單的狀態，不管有多少重複的，狀態沒影響

				// 解析消息
				Map<String, Object> body = FastJsonConvertUtil.convertJSONToObject(msgBody, Map.class);
				String orderId = (String) body.get("orderId");
				String userId = (String) body.get("userId");
				String status = (String)body.get("status");
				
				Date currentTime = new Date();

				// 判斷訂單狀態
				if(status.equals(OrderStatus.ORDER_PAYED.getValue())) {
					// 回调后更新訂單狀態
					int count  = orderMapper.updateOrderStatus(orderId, status, "admin", currentTime);
					// 更新订单状态成功了之后要配发物流，創建包裹
					if(count == 1) {
						// 發順序消息，創建包裹（第一個消息）並發送物流（第二個消息），他們兩個消息應該順序地執行
						orderService.sendOrderlyMessage4Pkg(userId, orderId);
					}
				
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				return ConsumeConcurrentlyStatus.RECONSUME_LATER;	
			}
            return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;	
		}
		
	}
	
}
