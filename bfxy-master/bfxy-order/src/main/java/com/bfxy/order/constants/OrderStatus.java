package com.bfxy.order.constants;

public enum OrderStatus {

    ORDER_CREATED("1"),//创建成功

    ORDER_PAYED("2"),//支付成功

    ORDER_FAIL("3");//失败

    private String status;//状态

    private OrderStatus(String status) {
        this.status = status;
    }

    public String getValue() {
        return status;
    }
}
