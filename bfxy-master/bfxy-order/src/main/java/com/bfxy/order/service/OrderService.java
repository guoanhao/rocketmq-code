package com.bfxy.order.service;

public interface OrderService {

	boolean createOrder(String cityId, String platformId, String userId, String supplierId, String goodsId);

	// 發順序消息，創建包裹
	void sendOrderlyMessage4Pkg(String userId, String orderId);

	
	
}
