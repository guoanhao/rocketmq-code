/*==============================================================*/
/* Table: broker_message_log                                    */
/*==============================================================*/
create table broker_message_log
(
   message_id           varchar(32) not null,
   message              varchar(400),
   try_count            int(5),
   status               varchar(10),
   next_retry           timestamp,
   create_time          timestamp not null,
   update_time          timestamp not null,
   primary key (message_id)
);

/*==============================================================*/
/* Table: t_customer_account                                     */
/*==============================================================*/
create table t_customer_account
(
   account_id           varchar(32) not null,
   account_no           varchar(32),
   date_time            timestamp,
   current_balance      decimal(15,2),
   version              int(10),
   create_time          timestamp not null,
   update_time          timestamp not null,
   primary key (account_id)
);

/*==============================================================*/
/* Table: t_order                                               */
/*==============================================================*/
create table t_order
(
   order_id             varchar(32) not null comment "订单ID",
   order_type           varchar(10) comment "订单类型",
   city_id              varchar(32) comment "城市ID",
   platform_id          varchar(32) comment "平台ID",
   user_id              varchar(32) comment "用户ID",
   supplier_id          varchar(32) comment "商户ID",
   goods_id             varchar(32) comment "商品ID",
   order_status         varchar(32) comment "订单状态",
   remark               varchar(200) comment "备注",
   create_by            varchar(50) not null comment "创建人",
   create_time          timestamp not null comment "创建时间",
   update_by            varchar(50) not null comment "更新人",
   update_time          timestamp not null comment "更新时间",
   primary key (order_id)
);

/*==============================================================*/
/* Index: order_index                                           */
/*==============================================================*/
create unique index order_index on t_order
(
   order_id
);

/*==============================================================*/
/* Table: t_package                                             */
/*==============================================================*/
create table t_package
(
   package_id           varchar(32) not null,
   order_id             varchar(32),
   supplier_id          varchar(32),
   address_id           varchar(32),
   remark               varchar(40),
   package_status       varchar(10),
   create_time          timestamp not null,
   update_time          timestamp not null,
   primary key (package_id)
);

/*==============================================================*/
/* Table: t_platform_account                                    */
/*==============================================================*/
create table t_platform_account
(
   account_id           varchar(32) not null,
   account_no           varchar(32),
   date_time            timestamp,
   current_balance      decimal(15,2) comment '当前余额',
   version              int(10),
   create_time          timestamp not null,
   update_time          timestamp not null,
   primary key (account_id)
);

/*==============================================================*/
/* Table: t_store                                               */
/*==============================================================*/
create table t_store
(
   store_id             varchar(32) not null,
   goods_id             varchar(32),
   supplier_id          varchar(32),
   goods_name           varchar(40),
   store_count          int(10) not null,
   version              int(10) not null,
   create_by            varchar(50) not null,
   create_time          timestamp not null,
   update_by            varchar(50) not null,
   update_time          timestamp not null,
   primary key (store_id)
);

