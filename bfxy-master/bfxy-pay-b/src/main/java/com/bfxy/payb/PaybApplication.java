package com.bfxy.payb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaybApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(PaybApplication.class, args);
		
	}
	
}
