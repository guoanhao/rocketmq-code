package com.bfxy.store.api;

public interface HelloServiceApi {
    //提供一个API方式的一个引入，
    // 也就是说我们的order服务想要试用store里面的服务，
    // 就需要把这个API引入到order项目里
    String sayHello(String name);

}
