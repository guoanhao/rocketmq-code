package com.bfxy.store.api;

import java.util.Date;

public interface StoreServiceApi {

    /**
     * 查询版本号
     *
     * @param supplierId 商户ID
     * @param goodsId    商品ID
     * @return
     */
    int selectVersion(String supplierId, String goodsId);

    /**
     * 更新库存
     *
     * @param version    版本号
     * @param supplierId 商户ID
     * @param goodsId    商品ID
     * @param updateBy   更新人
     * @param updateTime 更新时间
     * @return
     */
    int updateStoreCountByVersion(int version, String supplierId, String goodsId, String updateBy, Date updateTime);

    /**
     * 查询库存
     *
     * @param supplierId 商户ID
     * @param goodsId    商品ID
     * @return
     */
    int selectStoreCount(String supplierId, String goodsId);
}
